<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'city';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\User', 'city_live', 'id_city');
    }

    public function companyCity(){
        return $this->belongsTo('App\CompanyHasCity', 'city_id', 'id_city');
    }
}
