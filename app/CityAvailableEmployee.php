<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityAvailableEmployee extends Model
{
    protected $table = 'city_available_employee';
    public $timestamps = false;
}
