<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientHasEmployeePromoted extends Model
{
    protected $table = 'client_has_employee_promoted';
    public $timestamps = false;
}
