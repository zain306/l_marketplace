<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientNeed extends Model
{
    protected $table = 'client_need';
    public $timestamps = false;

    public function clientNeedCities(){
        return $this->hasMany('App\ClientNeedHasCity', 'client_need_id','id_client_need');
    }

    public function clientNeedTechnologies(){
        return $this->hasMany('App\ClientNeedHasTechnology', 'client_need_id','id_client_need');
    }
}
