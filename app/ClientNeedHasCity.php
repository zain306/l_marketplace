<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientNeedHasCity extends Model
{
    protected $table = 'client_need_has_city';
    public $timestamps = false;

    public function clientNeed(){
        return $this->belongsTo('App\ClientNeed', 'id_client_need', 'client_need_id');
    }

    public function city(){
        return $this->hasOne('App\City', 'id_city','city_id');
    }
}
