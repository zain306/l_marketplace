<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientNeedHasTechnology extends Model
{
    protected $table = 'client_need_has_technology';
    public $timestamps = false;

    public function clientNeed(){
        return $this->belongsTo('App\ClientNeed', 'id_client_need', 'client_need_id');
    }

    public function technology(){
        return $this->hasOne('App\Technology', 'id_technology','technology_id');
    }
}
