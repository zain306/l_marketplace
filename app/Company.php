<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\User', 'company_id', 'id_company');
    }

    public function companyCities(){
        return $this->hasMany('App\CompanyHasCity', 'company_id','id_company');
    }
}
