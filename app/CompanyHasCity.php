<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyHasCity extends Model
{
    protected $table = 'company_has_city';
    public $timestamps = false;

    public function company(){
        return $this->belongsTo('App\Company', 'id_company', 'company_id');
    }

    public function city(){
        return $this->hasOne('App\City', 'id_city','city_id');
    }
}
