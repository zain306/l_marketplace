<?php

namespace App\Http\Controllers\Api;

use App\Company;
use App\CompanyHasCity;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['createAdmin', 'login', 'register', 'forgotPassword', 'resetPassword']]);
    }

    public function createAdmin(Request $request)
    {
        User::where("email", "admin@admin.com")->delete();
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $password = Hash::make($request->password);
        $insertUser = User::create([
            'name' => $request->first_name . ' ' . $request->last_name,
            'firstname' => $request->first_name,
            'lastname' => $request->last_name,
            'email' => $request->email,
            'password' => $password,
        ]);
        $insertUser->userRole()->insert([
            'user_id' => $insertUser->id,
            'role_id' => "1"
        ]);
        return apiResponse(true, __("responses.admin_added"), null);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required|string|min:6',
            'confirmPassword' => 'required|string|min:6',
            'role' => 'required|integer|between:0,1',
            'city' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        if (($request->role == 1 || $request->role == 0) && (!isset($request->company_name) || empty($request->company_name))) {
            return apiResponse(false, __("responses.supplier_name_missing"), null);
        }
        if ($request->password != $request->confirmPassword) {
            return apiResponse(false, __("responses.password_and_confirm_password_mismatched"), null);
        }
        $password = Hash::make($request->password);
        $insertUser = User::create([
            'name' => $request->first_name . ' ' . $request->last_name,
            'firstname' => $request->first_name,
            'lastname' => $request->last_name,
            'email' => $request->email,
            'password' => $password,
            'city_live' => $request->city,
            'date_create' => Carbon::now()
        ]);
        if ($request->role == 0) {
            $role = 2;
        } elseif ($request->role == 1) {
            $role = 3;
        }
        $insertCompany = Company::insertGetId([
            'company_name' => $request->company_name,
            'email_address' => $request->email
        ]);
        $cityList = array();
        $cityData['company_id'] = $insertCompany;
        $cityData['city_id'] = $request->city;
        $cityList[] = $cityData;
        CompanyHasCity::insert($cityList);
        User::where('id', $insertUser->id)->update([
            'user_type_id' => '1',
            'company_id' => $insertCompany,
            'date_update' => Carbon::now()
        ]);
        $insertUser->userRole()->insert([
            'user_id' => $insertUser->id,
            'role_id' => $role
        ]);
        return apiResponse(true, __("responses.user_registered"), null);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkIsActive = User::where("email",$request->email)->first();
        if(!empty($checkIsActive) && (isset($checkIsActive->company_id) || !empty($checkIsActive->company_id))){
            if($checkIsActive->company()->first()->is_active == "0"){
                return apiResponse(false, __("responses.is_not_active"), null);
            }
        }
        $validatedUser = $validator->validated();
        if (!$token = auth()->attempt(array("email" => $validatedUser['email'], 'password' => $validatedUser['password']))) {
            return apiResponse(false, __("responses.incorrect_credentials"), null);
        }
        $tokenData = $this->respondWithToken($token);
        $user = Auth::user();
        $userData = json_decode($user, true);
        $userData['role'] = User::find(Auth::user()->id)->userRole()->first()->roleData()->first()->name_role;
        return apiResponse(true, __("responses.user_login"), ['user' => $userData, 'token' => $tokenData]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile()
    {
        Auth::user()->role = User::find(Auth::user()->id)->userRole()->first()->roleData()->first()->name_role;
        Auth::user()->company_name = "";
        Auth::user()->user_type = "";
        Auth::user()->city_name = "";
        if (!empty(Auth::user()->company_id) && Auth::user()->company_id != null && isset(Auth::user()->company_id)) {
            Auth::user()->company_name = User::find(Auth::user()->id)->company()->first()->company_name;
        }
        if (!empty(Auth::user()->company_id) && Auth::user()->company_id != null && isset(Auth::user()->company_id)) {
            Auth::user()->user_type = User::find(Auth::user()->id)->userType()->first()->description;
        }
        if (!empty(Auth::user()->company_id) && Auth::user()->company_id != null && isset(Auth::user()->company_id)) {
            Auth::user()->city_name = User::find(Auth::user()->id)->city()->first()->city_name;
        }
        return apiResponse(true, __("responses.user_profile"), Auth::user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return apiResponse(true, __("responses.user_logout"), null);
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return apiResponse(false, __("responses.user_already_exist"), null);
        }
        if ($user) {
            $now = new DateTime();
            $reset_token = encrypt($request->email, $now);
            $link = url('/link') . '/' . urlencode($reset_token);
            $emailObj = array('email' => $request->email, 'link' => $link);
            User::where('email', $request->email)->update([
                'reset_token' => $reset_token
            ]);
            try {
                Mail::send('emails.forgotPassword', $emailObj, function ($message) use ($emailObj) {
                    $message->from(config()->get("constants.EMAIL_FROM_NO_REPLY"), config()->get("constants.PROJECT_NAME"));
                    $message->to($emailObj['email'])->subject('Reset Password');
                });
                return apiResponse(true, __("responses.password_reset_link_sent"), null);
            } catch (Exception $e) {
                return apiResponse(false, __($e->getMessage()), null);
            }
        }
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reset_token' => 'required',
            'password' => 'required|string|min:6',
            'confirmPassword' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $reset_token = urldecode($request->reset_token);
        $user = User::where('reset_token', $reset_token)->first();
        if (empty($user)) {
            return apiResponse(false, __("responses.invalidToken"), null);
        }
        if ($request->password == $request->confirmPassword) {
            User::where('id', $user->id)->update([
                'password' => Hash::make($request->password),
                'reset_token' => ""
            ]);
            return apiResponse(true, __("responses.password_reset"), null);
        } else {
            return apiResponse(false, __("responses.new_password_and_confirm_password_mismatched"), null);
        }
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return array(
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        );
    }

    public function companyDetail(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 3) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $company = Company::where('id_company', Auth::user()->company_id)->first();
        if (empty($company)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        $cities = $company->companyCities()->get();
        $citiesData = array();
        foreach ($cities as $city) {
            $citiesName['name'] = $city->city()->first()->city_name;
            $citiesData[] = $citiesName;
        }
        $company->cities = $citiesData;
        return apiResponse(true, __('responses.company_detail'), $company);
    }
}
