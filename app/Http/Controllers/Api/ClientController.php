<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\ClientNeed;
use App\ClientNeedHasCity;
use App\ClientNeedHasTechnology;
use App\Http\Controllers\Controller;
use App\Http\Resources\CityResource;
use App\Http\Resources\ClientNeedResource;
use App\Http\Resources\JobSectorResource;
use App\Http\Resources\TechnologyResource;
use App\JobSector;
use App\NeedResource;
use App\NeedResourceHasTechnology;
use App\Technology;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function createNeed(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 2) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'job_sector_id' => 'required|integer',
            'remote_work' => 'required|integer',
            'start_date' => 'required|date_format:Y-m-d H:i:s',
            'end_date' => 'required|date_format:Y-m-d H:i:s',
            'selected_cities' => 'required|array',
            'selected_technologies' => 'required|array',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkJobSector = JobSector::where('id_job_sector', $request->job_sector_id)->where('deleted', '0')->first();
        if (empty($checkJobSector)) {
            return apiResponse(false, __("responses.job_sector_not_found"), null);
        }
        $createNeed = ClientNeed::insertGetId([
            'title' => $request->title,
            'description' => $request->description,
            'job_sector_id' => $checkJobSector->id_job_sector,
            'user_id' => Auth::user()->id,
            'remote_work' => $request->remote_work,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'date_create' => Carbon::now(),
            'date_update' => Carbon::now(),
        ]);
        ClientNeedHasCity::where('client_need_id', $createNeed)->delete();
        $cityList = array();
        for ($i = 0; $i < sizeof($request->selected_cities); $i++) {
            $cityData['client_need_id'] = $createNeed;
            $cityData['city_id'] = $request->selected_cities[$i];
            $cityList[] = $cityData;
        }
        ClientNeedHasCity::insert($cityList);
        ClientNeedHasTechnology::where('client_need_id', $createNeed)->delete();
        $technologyList = array();
        for ($i = 0; $i < sizeof($request->selected_technologies); $i++) {
            $technologyData['client_need_id'] = $createNeed;
            $technologyData['technology_id'] = $request->selected_technologies[$i];
            $technologyList[] = $technologyData;
        }
        ClientNeedHasTechnology::insert($technologyList);
        return apiResponse(true, __('responses.need_created'), null);
    }

    public function needList(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 2) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $clientNeedData = ClientNeed::where('user_id', Auth::user()->id)->get();
        $needs = ClientNeedResource::collection($clientNeedData);
        return apiResponse(true, __('responses.client_need_list'), $needs);
    }

    public function needDetail(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 2) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'need_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $need = ClientNeed::where('id_client_need', $request->need_id)->where('user_id', Auth::user()->id)->first();
        if (empty($need)) {
            return apiResponse(false, __('responses.need_not_found'), null);
        }
        $cities = $need->clientNeedCities()->get();
        $citiesData = array();
        foreach ($cities as $city) {
            $citiesData[] = $city->city()->first()->id_city;
        }
        $need->selected_cities = $citiesData;
        $need->cities = CityResource::collection(City::all());
        $technologies = $need->clientNeedTechnologies()->get();
        $technologiesData = array();
        foreach ($technologies as $technology) {
            $technologiesData[] = $technology->technology()->first()->id_technology;
        }
        $need->selected_technologies = $technologiesData;
        $need->technologies = TechnologyResource::collection(Technology::where('deleted', '0')->get());
        $need->job_sectors = JobSectorResource::collection(JobSector::where('deleted', '0')->get());
        return apiResponse(true, __('responses.need_detail'), $need);
    }

    public function updateNeed(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 2) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'id_client_need' => 'required',
            'title' => 'required',
            'description' => 'required',
            'job_sector_id' => 'required|integer',
            'remote_work' => 'required|integer',
            'start_date' => 'required|date_format:Y-m-d H:i:s',
            'end_date' => 'required|date_format:Y-m-d H:i:s',
            'selected_cities' => 'required|array',
            'selected_technologies' => 'required|array',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $need = ClientNeed::where('id_client_need', $request->id_client_need)->where('user_id', Auth::user()->id)->first();
        if (empty($need)) {
            return apiResponse(false, __('responses.need_not_found'), null);
        }
        $checkJobSector = JobSector::where('id_job_sector', $request->job_sector_id)->where('deleted', '0')->first();
        if (empty($checkJobSector)) {
            return apiResponse(false, __("responses.job_sector_not_found"), null);
        }
        $updateClientNeed = ClientNeed::where('id_client_need', $request->id_client_need)->update([
            'title' => $request->title,
            'description' => $request->description,
            'job_sector_id' => $checkJobSector->id_job_sector,
            'user_id' => Auth::user()->id,
            'remote_work' => $request->remote_work,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'date_update' => Carbon::now(),
        ]);
        ClientNeedHasCity::where('client_need_id', $request->id_client_need)->delete();
        $cityList = array();
        for ($i = 0; $i < sizeof($request->selected_cities); $i++) {
            $cityData['client_need_id'] = $request->id_client_need;
            $cityData['city_id'] = $request->selected_cities[$i];
            $cityList[] = $cityData;
        }
        ClientNeedHasCity::insert($cityList);
        ClientNeedHasTechnology::where('client_need_id', $request->id_client_need)->delete();
        $technologyList = array();
        for ($i = 0; $i < sizeof($request->selected_technologies); $i++) {
            $technologyData['client_need_id'] = $request->id_client_need;
            $technologyData['technology_id'] = $request->selected_technologies[$i];
            $technologyList[] = $technologyData;
        }
        ClientNeedHasTechnology::insert($technologyList);
        return apiResponse(true, __('responses.need_updated'), null);
    }

    public function updateNeedStatus(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 2) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'id_client_need' => 'required',
            'is_active' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $need = ClientNeed::where('id_client_need', $request->id_client_need)->where('user_id', Auth::user()->id)->first();
        if (empty($need)) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        ClientNeed::where('id_client_need', $request->id_client_need)->update([
            'is_active' => $request->is_active,
            'date_update' => Carbon::now(),
        ]);
        return apiResponse(true, __('responses.need_status_updated'), null);
    }

    public function createNeedResource(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 2) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'client_need_id' => 'required|integer',
            'year_expirence' => 'required|integer',
            'past_expirence' => 'required|integer',
            'seniority' => 'required|integer',
            'day_rate_max' => 'required|numeric',
            'selected_technologies' => 'required|array',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkNeed = ClientNeed::where('id_client_need',$request->client_need_id)->where('user_id',Auth::user()->id)->first();
        if(empty($checkNeed)){
            return apiResponse(false, __("responses.need_not_found"), null);            
        }
        $createNeedResource = NeedResource::insertGetId([
            'year_expirence' => $request->year_expirence,
            'client_need_id' => $request->client_need_id,
            'past_expirence' => $request->past_expirence,
            'seniority' => $request->seniority,
            'day_rate_max' => $request->day_rate_max,
        ]);
        NeedResourceHasTechnology::where('need_resource_id_need_resource',$createNeedResource)->delete();
        $technologyList = array();
        for ($i = 0; $i < sizeof($request->selected_technologies); $i++) {
            $technologyData['need_resource_id_need_resource'] = $createNeedResource;
            $technologyData['technology_id_technology'] = $request->selected_technologies[$i];
            $technologyList[] = $technologyData;
        }
        NeedResourceHasTechnology::insert($technologyList);
        return apiResponse(true, __('responses.need_resource_created'), null);
    }

    public function updateNeedResource(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 2) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'id_need_resource' => 'required',
            'year_expirence' => 'required|integer',
            'past_expirence' => 'required|integer',
            'seniority' => 'required|integer',
            'day_rate_max' => 'required|numeric',
            'selected_technologies' => 'required|array',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkNeedResource = NeedResource::where('id_need_resource',$request->id_need_resource)->first();
        if(empty($checkNeedResource)){
            return apiResponse(false, __("responses.need_resource_not_found"), null);            
        }
        $updateNeedResource = NeedResource::where('id_need_resource',$request->id_need_resource)->update([
            'year_expirence' => $request->year_expirence,
            'past_expirence' => $request->past_expirence,
            'seniority' => $request->seniority,
            'day_rate_max' => $request->day_rate_max,
        ]);
        NeedResourceHasTechnology::where('need_resource_id_need_resource',$request->id_need_resource)->delete();
        $technologyList = array();
        for ($i = 0; $i < sizeof($request->selected_technologies); $i++) {
            $technologyData['need_resource_id_need_resource'] = $request->id_need_resource;
            $technologyData['technology_id_technology'] = $request->selected_technologies[$i];
            $technologyList[] = $technologyData;
        }
        NeedResourceHasTechnology::insert($technologyList);
        return apiResponse(true, __('responses.need_resource_updated'), null);
    }
}
