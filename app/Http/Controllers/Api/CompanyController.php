<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Company;
use App\CompanyHasCity;
use App\Http\Controllers\Controller;
use App\Http\Resources\CityResource;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\CompanySubscriptionResource;
use App\Http\Resources\CompanyUsersResource;
use App\Plan;
use App\PlanPurchased;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function list(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $companiesData = Company::all();
        $companies = CompanyResource::collection($companiesData);
        return apiResponse(true, __('responses.company_list'), $companies);
    }

    public function detail(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $company = Company::where('id_company', $request->company_id)->first();
        if (empty($company)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        $cities = $company->companyCities()->get();
        $citiesData = array();
        foreach ($cities as $city) {
            $citiesData[] = $city->city()->first()->id_city;
        }
        $company->selected_cities = $citiesData;
        $company->cities = CityResource::collection(City::all());
        return apiResponse(true, __('responses.company_detail'), $company);
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1 && $user->userRole()->first()->role_id != 3) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'company_name' => 'required',
            'email_address' => 'required|email',
            'vat_number' => 'required|string|max:30',
            'date_of_fondation' => 'required|date_format:Y-m-d H:i:s',
            'number_of_employees' => 'required|integer',
            'number_of_external_resources' => 'required|integer',
            'address_main_office' => 'required',
            'path_registration_document' => 'required',
            'id_city_head_quarter' => 'required|integer',
            'is_active' => 'required|integer',
            'selected_cities' => 'required|array',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkCompany = Company::where('id_company', $request->company_id)->first();
        if (empty($checkCompany)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        Company::where('id_company', $request->company_id)->update([
            'company_name' => $request->company_name,
            'email_address' => $request->email_address,
            'vat_number' => $request->vat_number,
            'date_of_fondation' => $request->date_of_fondation,
            'number_of_employees' => $request->number_of_employees,
            'number_of_external_resources' => $request->number_of_external_resources,
            'address_main_office' => $request->address_main_office,
            'path_registration_document' => $request->path_registration_document,
            'id_city_head_quarter' => $request->id_city_head_quarter,
            'is_active' => $request->is_active,
        ]);
        $cityList = array();
        for ($i = 0; $i < sizeof($request->selected_cities); $i++) {
            $cityData['company_id'] = $request->company_id;
            $cityData['city_id'] = $request->selected_cities[$i];
            $cityList[] = $cityData;
        }
        CompanyHasCity::where('company_id', $request->company_id)->delete();
        CompanyHasCity::insert($cityList);
        return apiResponse(true, __('responses.company_updated'), null);
    }

    public function users(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $company = Company::where('id_company', $request->company_id)->first();
        if (empty($company)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        $cities = $company->companyCities()->get();
        $citiesData = array();
        foreach ($cities as $city) {
            $citiesData[] = $city->city()->first()->id_city;
        }
        $company->selected_cities = $citiesData;
        $usersData = User::where('company_id', $request->company_id)->get();
        $users = CompanyUsersResource::collection($usersData);
        $company->users = $users;
        return apiResponse(true, __('responses.company_users'), $company);
    }

    public function subscriptionList(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $company = Company::where('id_company', $request->company_id)->first();
        if (empty($company)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        $cities = $company->companyCities()->get();
        $citiesData = array();
        foreach ($cities as $city) {
            $citiesData[] = $city->city()->first()->id_city;
        }
        $company->selected_cities = $citiesData;
        $subscriptionsData = PlanPurchased::where('company_id', $request->company_id)->get();
        $subscriptions = CompanySubscriptionResource::collection($subscriptionsData);
        $company->subscriptions_list = $subscriptions;
        return apiResponse(true, __('responses.subscriptions_list'), $company);
    }

    public function subscriptionDetail(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'subscription_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $company = Company::where('id_company', $request->company_id)->first();
        if (empty($company)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        $subscriptionPurchased = PlanPurchased::where('plan_id', $request->subscription_id)->where('company_id', $request->company_id)->first();
        if (empty($subscriptionPurchased)) {
            return apiResponse(false, __('responses.subscription_not_found'), null);
        }
        $cities = $company->companyCities()->get();
        $citiesData = array();
        foreach ($cities as $city) {
            $citiesData[] = $city->city()->first()->id_city;
        }
        $company->selected_cities = $citiesData;
        $company->subscription_detail = $subscriptionPurchased;
        if(!empty($subscriptionPurchased->plan()->first())){
            $company->subscription_detail->plan = $subscriptionPurchased->plan()->first();
        }
        return apiResponse(true, __('responses.subscription_detail'), $company);
    }
}
