<?php

namespace App\Http\Controllers\Api;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeResource;
use App\TechnologyHasUser;
use App\TechnologyUserLinkType;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function add(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1 && ($user->userRole()->first()->role_id != 3 && $user->user_type != 1)) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required|string|min:6',
            'company_id' => 'required|integer',
            'city' => 'required|integer',
            'day_rate' => 'required|integer',
            'day_cost' => 'required|integer',
            'technology' => 'required|array'
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkCompany = Company::where('id_company', $request->company_id)->first();
        if (empty($checkCompany)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        $insertEmployee = User::insertGetId([
            'name' => $request->name,
            'firstname' => $request->first_name,
            'lastname' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'company_id' => $request->company_id,
            'user_type_id' => '2',
            'city_live' => $request->city,
            'day_rate' => $request->day_rate,
            'day_cost' => $request->day_cost,
            'date_create' => Carbon::now()
        ]);
        foreach ($request->technology as $technology) {
            $insertTechnologyUserTypeLink = TechnologyUserLinkType::insertGetId([
                'description' => $technology['description'],
                'date_create' => Carbon::now()
            ]);
            TechnologyHasUser::insert([
                'technology_id' => $technology['id'],
                'user_id' => $insertEmployee,
                'technology_user_link_type_id' => $insertTechnologyUserTypeLink
            ]);
        }
        return apiResponse(true, __('responses.employee_inserted'), null);
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1 && ($user->userRole()->first()->role_id != 3 && $user->user_type != 1)) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required',
            'name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'company_id' => 'required|integer',
            'city' => 'required|integer',
            'day_rate' => 'required|integer',
            'day_cost' => 'required|integer',
            'technology' => 'required|array'
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkCompany = Company::where('id_company', $request->company_id)->first();
        if (empty($checkCompany)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        $checkEmployee = User::where('id', $request->employee_id)->where('deleted', '0')->first();
        if (empty($checkEmployee)) {
            return apiResponse(false, __('responses.employee_not_found'), null);
        }
        $checkEmployeeOfCompany = User::where('id', $request->employee_id)->where('user_type_id', '2')->where('company_id', $request->company_id)->first();
        if (empty($checkEmployeeOfCompany)) {
            return apiResponse(false, __('responses.company_not_allowed'), null);
        }
        User::where('id', $request->employee_id)->update([
            'name' => $request->name,
            'firstname' => $request->first_name,
            'lastname' => $request->last_name,
            'company_id' => $request->company_id,
            'user_type_id' => '2',
            'city_live' => $request->city,
            'day_rate' => $request->day_rate,
            'day_cost' => $request->day_cost,
            'date_update' => Carbon::now()
        ]);
        foreach ($request->technology as $technology) {
            $checkTechnologyUser = TechnologyHasUser::where('user_id', $request->employee_id)->where('technology_id', $technology['id'])->first();
            TechnologyUserLinkType::where('id_technology_user_link_type', $checkTechnologyUser->technology_user_link_type_id)->update([
                'description' => $technology['description'],
                'date_update' => Carbon::now()
            ]);
        }
        return apiResponse(true, __('responses.employee_updated'), null);
    }

    public function list(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1 && ($user->userRole()->first()->role_id != 3 && $user->user_type != 1)) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'company_id' => 'required'
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkCompany = Company::where('id_company', $request->company_id)->first();
        if (empty($checkCompany)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        $getEmployeesList = User::where('company_id', $request->company_id)->where('user_type_id', '2')->where('deleted','0')->orderBy('id', "DESC")->get();
        $employeesList = array();
        foreach ($getEmployeesList as $employee) {
            $employeeData['id'] = $employee->id;
            $employeeData['name'] = $employee->name;
            $employeeData['firstname'] = $employee->firstname;
            $employeeData['lastname'] = $employee->lastname;
            $employeeData['email'] = $employee->email;
            $employeesList[] = $employeeData;
        }
        $employees = EmployeeResource::collection($employeesList);
        return apiResponse(true, __('responses.employee_retreived'), $employees);
    }

    public function detail(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1 && ($user->userRole()->first()->role_id != 3 && $user->user_type != 1)) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'company_id' => 'required',
            'employee_id' => 'required'
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkCompany = Company::where('id_company', $request->company_id)->first();
        if (empty($checkCompany)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        $getEmployee = User::where('company_id', $request->company_id)->where('user_type_id', '2')->where('id', $request->employee_id)->where('deleted','0')->first();
        if (empty($getEmployee)) {
            return apiResponse(false, __('responses.employee_not_found'), null);
        }
        $technologies = TechnologyHasUser::query();
        $technologies->join('technology', function ($join) {
            $join->on('technology.id_technology', '=', 'technology_has_user.technology_id');
        });
        $technologies->join('technology_user_link_type', function ($join1) {
            $join1->on('technology_user_link_type.id_technology_user_link_type', '=', 'technology_has_user.technology_user_link_type_id');
        });
        $technolgiesD = $technologies->where('user_id', $request->employee_id)->get();
        $technolgiesData = array();
        foreach ($technolgiesD as $technology) {
            $technologyList['name'] = $technology->name_technology;
            $technologyList['description'] = $technology->description;
            $technolgiesData[] = $technologyList;
        }
        $employeeData['id'] = $getEmployee->id;
        $employeeData['name'] = $getEmployee->name;
        $employeeData['firstname'] = $getEmployee->firstname;
        $employeeData['lastname'] = $getEmployee->lastname;
        $employeeData['email'] = $getEmployee->email;
        $employeeData['company'] = $getEmployee->company()->first()->company_name;
        $employeeData['city'] = $getEmployee->city()->first()->city_name;
        $employeeData['day_rate'] = $getEmployee->day_rate;
        $employeeData['day_cost'] = $getEmployee->day_cost;
        $employeeData['technolgies'] = $technolgiesData;
        // $employee = EmployeeResource::collection($employeeData);
        return apiResponse(true, __('responses.employee_data_retreived'), $employeeData);
    }

    public function delete(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1 && ($user->userRole()->first()->role_id != 3 && $user->user_type != 1)) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required',
            'company_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkCompany = Company::where('id_company', $request->company_id)->first();
        if (empty($checkCompany)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        $checkEmployee = User::where('id', $request->employee_id)->where('deleted', '0')->first();
        if (empty($checkEmployee)) {
            return apiResponse(false, __('responses.employee_not_found'), null);
        }
        $checkEmployeeOfCompany = User::where('id', $request->employee_id)->where('user_type_id', '2')->where('company_id', $request->company_id)->first();
        if (empty($checkEmployeeOfCompany)) {
            return apiResponse(false, __('responses.company_not_allowed'), null);
        }
        User::where('id', $request->employee_id)->update([
            'deleted' => "1",
            'date_deleted' => Carbon::now()
        ]);
        return apiResponse(true, __('responses.employee_deleted'), null);
    }
}
