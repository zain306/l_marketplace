<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\JobSectorResource;
use App\JobSector;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class JobSectorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function list(Request $request)
    {
        $jobSectors = JobSectorResource::collection(JobSector::where('deleted', '0')->get());
        return apiResponse(true, __('responses.jobSector_list'), $jobSectors);
    }

    public function add(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $insertJobSector = JobSector::insert([
            'name_job_sector' => $request->name,
            'date_create' => Carbon::now(),
        ]);
        if ($insertJobSector) {
            return apiResponse(true, __('responses.jobSector_inserted'), null);
        } else {
            return apiResponse(false, __("responses.something_wrong"), null);
        }
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'job_sector_id' => 'required',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkJobSector = JobSector::where('id_job_sector', $request->job_sector_id)->where('deleted', '0')->first();
        if (!$checkJobSector) {
            return apiResponse(false, __('responses.invalid_jobSector'), null);
        }
        JobSector::where('id_job_sector', $request->job_sector_id)->update([
            'name_job_sector' => $request->name,
            'date_update' => Carbon::now(),
        ]);
        return apiResponse(true, __('responses.jobSector_updated'), null);
    }

    public function delete(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'job_sector_id' => 'required'
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkJobSector = JobSector::where('id_job_sector', $request->job_sector_id)->where('deleted', '0')->first();
        if (!$checkJobSector) {
            return apiResponse(false, __('responses.invalid_jobSector'), null);
        }
        JobSector::where('id_job_sector', $request->job_sector_id)->update([
            'deleted' => "1",
            'date_deleted' => Carbon::now(),
            'date_update' => Carbon::now(),
        ]);
        return apiResponse(true, __('responses.jobSector_deleted'), null);
    }
}
