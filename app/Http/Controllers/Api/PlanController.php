<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PlanResource;
use App\Plan;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PlanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function list(Request $request)
    {
        $plans = PlanResource::collection(Plan::where('deleted', '0')->get());
        return apiResponse(true, __('responses.plan_list'), $plans);
    }

    public function add(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'price' => 'required',
            'number_token' => 'required|integer',
            'description' => 'required',
            'date_publish' => 'required|date_format:Y-m-d H:i:s',
            'date_expired' => 'required|date_format:Y-m-d H:i:s',
            'buy_limit' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $insertPlan = Plan::insert([
            'title' => $request->title,
            'price' => $request->price,
            'number_token' => $request->number_token,
            'description' => $request->description,
            'date_publish' => $request->date_publish,
            'date_expired' => $request->date_expired,
            'buy_limit' => $request->buy_limit,
            'date_created' => Carbon::now(),
        ]);
        if($insertPlan){
            return apiResponse(true, __('responses.plan_inserted'), null);
        }
        else{
            return apiResponse(false, __("responses.something_wrong"), null);
        }
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'plan_id' => 'required',
            'title' => 'required',
            'price' => 'required',
            'number_token' => 'required|integer',
            'description' => 'required',
            'date_publish' => 'required|date_format:Y-m-d H:i:s',
            'date_expired' => 'required|date_format:Y-m-d H:i:s',
            'buy_limit' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkPlan = Plan::where('id_plan',$request->plan_id)->where('deleted', '0')->first();
        if(!$checkPlan){
            return apiResponse(false, __('responses.invalid_plan'), null);
        }
        Plan::where('id_plan',$request->plan_id)->update([
            'title' => $request->title,
            'price' => $request->price,
            'number_token' => $request->number_token,
            'description' => $request->description,
            'date_publish' => $request->date_publish,
            'date_expired' => $request->date_expired,
            'buy_limit' => $request->buy_limit
        ]);
        return apiResponse(true, __('responses.plan_updated'), null);
    }

    public function delete(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'plan_id' => 'required'
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkPlan = Plan::where('id_plan',$request->plan_id)->where('deleted', '0')->first();
        if(!$checkPlan){
            return apiResponse(false, __('responses.invalid_plan'), null);
        }
        Plan::where('id_plan',$request->plan_id)->update([
            'deleted' => "1",
            'date_deleted' => Carbon::now(),
        ]);
        return apiResponse(true, __('responses.plan_deleted'), null);
    }
}
