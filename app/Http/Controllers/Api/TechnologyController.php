<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TechnologyResource;
use App\Role;
use App\Technology;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TechnologyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function list(Request $request){
        $technology = TechnologyResource::collection(Technology::where('deleted', '0')->get());
        return apiResponse(true, __('responses.technology_list'), $technology);
    }

    public function add(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $insertTechnology = Technology::insert([
            'name_technology' => $request->name,
            'date_create' => Carbon::now(),
        ]);
        if($insertTechnology){
            return apiResponse(true, __('responses.technology_inserted'), null);
        }
        else{
            return apiResponse(false, __("responses.something_wrong"), null);
        }
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'technology_id' => 'required',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkTechnology = Technology::where('id_technology',$request->technology_id)->where('deleted', '0')->first();
        if(!$checkTechnology){
            return apiResponse(false, __('responses.invalid_technology'), null);
        }
        Technology::where('id_technology',$request->technology_id)->update([
            'name_technology' => $request->name,
            'date_update' => Carbon::now(),
        ]);
        return apiResponse(true, __('responses.technology_updated'), null);
    }

    public function delete(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'technology_id' => 'required',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkTechnology = Technology::where('id_technology',$request->technology_id)->where('deleted', '0')->first();
        if(!$checkTechnology){
            return apiResponse(false, __('responses.invalid_technology'), null);
        }
        Technology::where('id_technology',$request->technology_id)->update([
            'deleted' => "1",
            'date_deleted' => Carbon::now(),
        ]);
        return apiResponse(true, __('responses.technology_deleted'), null);
    }
}
