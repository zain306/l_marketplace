<?php

namespace App\Http\Controllers\Api;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Technology;
use App\TechnologyHasWorkingExperience;
use App\User;
use App\WorkingExperience;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WorkingExperienceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function add(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1 && ($user->userRole()->first()->role_id != 3 && $user->user_type != 1)) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'start_date' => 'required|date_format:Y-m-d H:i:s',
            'end_date' => 'required|date_format:Y-m-d H:i:s',
            'employee_id' => 'required|integer',
            'company_id' => 'required|integer',
            'technology' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkCompany = Company::where('id_company', $request->company_id)->first();
        if (empty($checkCompany)) {
            return apiResponse(false, __('responses.company_not_found'), null);
        }
        $checkEmployee = User::where('id', $request->employee_id)->where('deleted', '0')->first();
        if (empty($checkEmployee)) {
            return apiResponse(false, __('responses.employee_not_found'), null);
        }
        $checkEmployeeOfCompany = User::where('id', $request->employee_id)->where('user_type_id', '2')->where('company_id', $request->company_id)->first();
        if (empty($checkEmployeeOfCompany)) {
            return apiResponse(false, __('responses.employee_not_found'), null);
        }
        $checkTechnology = Technology::where('id_technology', $request->technology)->where('deleted', '0')->first();
        if (empty($checkTechnology)) {
            return apiResponse(false, __('responses.technology_not_found'), null);
        }
        $insertWorkingExperience = WorkingExperience::insertGetId([
            'title' => $request->title,
            'description' => $request->description,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'user_id' => $request->employee_id,
            'date_create' => Carbon::now()
        ]);
        TechnologyHasWorkingExperience::insert([
            'working_experience_id_working_experience' => $insertWorkingExperience,
            'technology_id_technology' => $request->technology
        ]);
        return apiResponse(true, __('responses.working_experience_inserted'), null);
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1 && ($user->userRole()->first()->role_id != 3 && $user->user_type != 1)) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'start_date' => 'required|date_format:Y-m-d H:i:s',
            'end_date' => 'required|date_format:Y-m-d H:i:s',
            'working_experience_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkWorkingExperience = WorkingExperience::where('id_working_experience', $request->working_experience_id)->where('deleted', '0')->first();
        if (empty($checkWorkingExperience)) {
            return apiResponse(false, __('responses.working_experience_not_found'), null);
        }
        WorkingExperience::where('id_working_experience', $request->working_experience_id)->update([
            'title' => $request->title,
            'description' => $request->description,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ]);
        return apiResponse(true, __('responses.working_experience_updated'), null);
    }

    public function detail(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1 && ($user->userRole()->first()->role_id != 3 && $user->user_type != 1)) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'working_experience_id' => 'required',
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $getWorkingExperience = WorkingExperience::where('id_working_experience', $request->working_experience_id)->where('deleted', '0')->first();
        if (empty($getWorkingExperience)) {
            return apiResponse(false, __('responses.working_experience_not_found'), null);
        }
        $technologyName = "";
        $technology = Technology::where('id_technology',$getWorkingExperience->experienceTechnology()->first()->technology_id_technology)->first();
        if(!empty($technology)){
            $technologyName = $technology->name_technology;
        }
        $workingExperience['id'] = $getWorkingExperience->id_working_experience;
        $workingExperience['title'] = $getWorkingExperience->title;
        $workingExperience['description'] = $getWorkingExperience->description;
        $workingExperience['start_date'] = $getWorkingExperience->start_date;
        $workingExperience['end_date'] = $getWorkingExperience->end_date;
        $workingExperience['technology'] = $technologyName;
        // $employee = EmployeeResource::collection($employeeData);
        return apiResponse(true, __('responses.working_experience_detail'), $workingExperience);
    }

    public function delete(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if ($user->userRole()->first()->role_id != 1 && ($user->userRole()->first()->role_id != 3 && $user->user_type != 1)) {
            return apiResponse(false, __('responses.not_accessible'), null);
        }
        $validator = Validator::make($request->all(), [
            'working_experience_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return apiResponse(false, __($validator->errors()->first()), null);
        }
        $checkWorkingExperience = WorkingExperience::where('id_working_experience', $request->working_experience_id)->where('deleted', '0')->first();
        if (empty($checkWorkingExperience)) {
            return apiResponse(false, __('responses.working_experience_not_found'), null);
        }
        WorkingExperience::where('id_working_experience', $request->working_experience_id)->update([
            'deleted' => "1",
            'date_deleted' => Carbon::now(),
        ]);
        return apiResponse(true, __('responses.working_experience_deleted'), null);
    }
}
