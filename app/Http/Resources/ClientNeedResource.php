<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientNeedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $cities = $this->clientNeedCities()->get();
        $citiesData = array();
        foreach($cities as $city){
            $citiesData[] = $city->city()->first()->city_name;
        }
        $technologies = $this->clientNeedTechnologies()->get();
        $technologiesData = array();
        foreach($technologies as $technology){
            $technologiesData[] = $technology->technology()->first()->name_technology;
        }
        return [
            'id_client_need' => $this->id_client_need,
            'user_id' => $this->user_id,
            'title' => $this->company_name,
            'description' => $this->email_address,
            'status' => $this->is_active,
            'selected_cities' => $citiesData,
            'selected_technolgies' => $technologiesData,
        ];
    }
}
