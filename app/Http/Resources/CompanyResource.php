<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $cities = $this->companyCities()->get();
        $citiesData = array();
        foreach($cities as $city){
            $citiesData[] = $city->city()->first()->city_name;
        }
        return [
            'company_id' => $this->id_company,
            'name' => $this->company_name,
            'email' => $this->email_address,
            'city' => $citiesData,
            'status' => $this->is_active,
        ];
    }
}
