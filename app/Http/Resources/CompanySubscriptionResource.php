<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanySubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $planName = "";
        if(!empty($this->plan()->first())){
            $planName = $this->plan()->first()->title;
        }
        return [
            'subscription_id' => $this->id_plan_purchased,
            'plan_id' => $this->plan_id,
            'plan_name' => $planName,
            'date_purchase' => $this->date_purchase,
        ];
    }
}
