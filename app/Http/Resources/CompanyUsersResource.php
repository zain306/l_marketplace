<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyUsersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $city_name = "";
        if(!empty($this->city()->first())){
            $city_name = $this->city()->first()->city_name;
        }
        return [
            'user_id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'city' => $city_name,
            'user_type' => $this->userType()->first()->description,
        ];
    }
}
