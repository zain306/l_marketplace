<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobSector extends Model
{
    protected $table = 'job_sector';
    public $timestamps = false;
}
