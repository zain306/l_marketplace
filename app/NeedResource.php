<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NeedResource extends Model
{
    protected $table = 'need_resource';
    public $timestamps = false;

    public function needResourceTechnologies(){
        return $this->hasMany('App\NeedResourceHasTechnology', 'need_resource_id_need_resource','id_need_resource');
    }
}
