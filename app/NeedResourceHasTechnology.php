<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NeedResourceHasTechnology extends Model
{
    protected $table = 'need_resource_has_technology';
    public $timestamps = false;
    
    public function technology(){
        return $this->hasOne('App\Technology', 'technology_id_technology','technology_id');
    }
}
