<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $table = 'plan';
    public $timestamps = false;

    public function planPurchased(){
        return $this->belongsTo('App\PlanPurchased', 'plan_id', 'id_plan');
    }
}
