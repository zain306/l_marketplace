<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanPurchased extends Model
{
    protected $table = 'plan_purchased';
    public $timestamps = false;

    public function plan(){
        return $this->hasOne('App\Plan', 'id_plan','plan_id');
    }
}
