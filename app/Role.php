<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';
    public $timestamps = false;

    public function roleUsers(){
        return $this->hasMany('App\RoleHasUser', 'role_id','id_role');
    }
}
