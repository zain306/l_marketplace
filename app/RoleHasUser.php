<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleHasUser extends Model
{
    protected $table = 'role_has_user';
    public $timestamps = false;

    public function users(){
        return $this->belongsTo('App\User', 'id', 'user_id');
    }

    public function role(){
        return $this->belongsTo('App\Role', 'id_role', 'role_id');
    }

    public function roleData(){
        return $this->hasOne('App\Role', 'id_role', 'role_id');
    }
}
