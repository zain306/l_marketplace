<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnologyHasUser extends Model
{
    protected $table = 'technology_has_user';
    public $timestamps = false;
}
