<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnologyHasWorkingExperience extends Model
{
    protected $table = 'technology_has_working_experience';
    public $timestamps = false;

    public function workingExperience(){
        return $this->belongsTo('App\WorkingExperience', 'id_working_experience', 'working_experience_id_working_experience');
    }
}
