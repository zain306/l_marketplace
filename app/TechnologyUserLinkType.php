<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnologyUserLinkType extends Model
{
    protected $table = 'technology_user_link_type';
    public $timestamps = false;
}
