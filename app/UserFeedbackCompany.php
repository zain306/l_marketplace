<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFeedbackCompany extends Model
{
    protected $table = 'user_feedback_company';
    public $timestamps = false;
}
