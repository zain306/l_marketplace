<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    protected $table = 'user_type';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\User', 'user_type_id', 'id_user_type');
    }
}
