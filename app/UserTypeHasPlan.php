<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTypeHasPlan extends Model
{
    protected $table = 'user_type_has_plan';
    public $timestamps = false;
}
