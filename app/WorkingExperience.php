<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkingExperience extends Model
{
    protected $table = 'working_experience';
    public $timestamps = false;

    public function experienceTechnology(){
        return $this->hasOne('App\TechnologyHasWorkingExperience', 'working_experience_id_working_experience','id_working_experience');
    }
}
