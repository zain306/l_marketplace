<?php

if (!function_exists('apiResponse')) {
    function apiResponse($success, $message, $data)
    {
        $response['success'] = $success;
        $response['message'] = $message;
        $response['data'] = $data;
        return response()->json($response);
    }
}