<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityAvailableEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_available_employee', function (Blueprint $table) {
            $table->integer('city_id')->index('fk_city_has_user_city1_idx');
            $table->integer('user_id')->index('fk_city_has_user_user1_idx');
            $table->primary(['city_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_available_employee');
    }
}
