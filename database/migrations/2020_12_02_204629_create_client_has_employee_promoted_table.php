<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientHasEmployeePromotedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_has_employee_promoted', function (Blueprint $table) {
            $table->increments('need_resource_id');
            $table->integer('user_id_employee')->index('fk_client_need_has_user_user1_idx')->comment('id dell\'utente di tipo employee promosso');
            $table->integer('user_author_promotion_id')->index('fk_client_has_employee_promoted_user1_idx')->comment('id dell\'utente che promuove l\'employee');
            $table->dateTime('date_promotion')->nullable();
            // $table->primary(['need_resource_id', 'user_id_employee'],'need_resource_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_has_employee_promoted');
    }
}
