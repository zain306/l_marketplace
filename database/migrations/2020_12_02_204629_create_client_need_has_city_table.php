<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientNeedHasCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_need_has_city', function (Blueprint $table) {
            $table->integer('client_need_id')->index('fk_client_need_has_city_client_need1_idx');
            $table->integer('city_id')->index('fk_client_need_has_city_city1_idx');
            $table->primary(['client_need_id', 'city_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_need_has_city');
    }
}
