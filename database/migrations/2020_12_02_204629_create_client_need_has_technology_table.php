<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientNeedHasTechnologyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_need_has_technology', function (Blueprint $table) {
            $table->integer('client_need_id')->index('fk_client_need_has_technology_client_need1_idx');
            $table->integer('technology_id')->index('fk_client_need_has_technology_technology1_idx');
            $table->primary(['client_need_id', 'technology_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_need_has_technology');
    }
}
