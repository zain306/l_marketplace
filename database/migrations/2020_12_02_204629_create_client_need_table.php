<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientNeedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_need', function (Blueprint $table) {
            $table->integer('id_client_need', true);
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->integer('job_sector_id')->index('fk_client_need_job_sector1_idx');
            $table->integer('user_id')->index('fk_client_need_user1_idx');
            $table->integer('remote_work')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->integer('is_active');
            $table->integer('deleted');
            $table->dateTime('date_delete')->nullable();
            $table->dateTime('date_create')->nullable();
            $table->dateTime('date_update')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_need');
    }
}
