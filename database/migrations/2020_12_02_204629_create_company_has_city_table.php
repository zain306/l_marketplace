<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyHasCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_has_city', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->index('fk_company_has_city_company_idx');
            $table->integer('city_id')->index('fk_company_has_city_city1_idx');
            $table->primary(['company_id', 'city_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_has_city');
    }
}
