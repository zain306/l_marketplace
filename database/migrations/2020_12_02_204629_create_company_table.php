<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->integer('id_company', true);
            $table->string('company_name')->nullable();
            $table->string('vat_number', 30)->nullable();
            $table->dateTime('date_of_fondation')->nullable();
            $table->integer('number_of_employees')->nullable();
            $table->integer('number_of_external_resources')->nullable();
            $table->string('address_main_office')->nullable();
            $table->string('path_registration_document')->nullable();
            $table->integer('id_city_head_quarter')->nullable()->index('fk_company_city1_idx');
            $table->integer('is_active')->default('0');
            $table->integer('deleted')->nullable();
            $table->dateTime('date_deleted')->nullable();
            $table->string('email_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
