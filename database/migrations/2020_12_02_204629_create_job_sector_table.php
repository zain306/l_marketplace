<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobSectorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_sector', function (Blueprint $table) {
            $table->integer('id_job_sector', true);
            $table->string('name_job_sector')->nullable();
            $table->integer('deleted')->nullable()->default(0);
            $table->dateTime('date_deleted')->nullable();
            $table->dateTime('date_create')->nullable()->useCurrent();
            $table->dateTime('date_update')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_sector');
    }
}
