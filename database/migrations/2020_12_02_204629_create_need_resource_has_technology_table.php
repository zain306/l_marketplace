<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNeedResourceHasTechnologyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('need_resource_has_technology', function (Blueprint $table) {
            $table->integer('need_resource_id_need_resource')->index('fk_need_resource_has_technology_need_resource1_idx');
            $table->integer('technology_id_technology')->index('fk_need_resource_has_technology_technology1_idx');
            // $table->primary(['need_resource_id_need_resource', 'technology_id_technology'],'need_resource_id_need_resource');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('need_resource_has_technology');
    }
}
