<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNeedResourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('need_resource', function (Blueprint $table) {
            $table->integer('id_need_resource', true);
            $table->integer('year_expirence')->nullable();
            $table->integer('client_need_id')->index('fk_need_resource_client_need1_idx');
            $table->integer('past_expirence')->nullable();
            $table->integer('seniority')->nullable();
            $table->float('day_rate_max', 10, 0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('need_resource');
    }
}
