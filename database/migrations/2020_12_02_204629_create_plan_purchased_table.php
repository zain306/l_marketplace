<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanPurchasedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_purchased', function (Blueprint $table) {
            $table->increments('id_plan_purchased');
            $table->integer('plan_id')->index('fk_plan_purchased_plan1_idx');
            $table->dateTime('date_purchase')->nullable();
            $table->integer('deleted')->nullable();
            $table->dateTime('date_deleted')->nullable();
            $table->integer('company_id')->index('fk_plan_purchased_company1_idx');
            $table->integer('user_id')->index('fk_plan_purchased_user1_idx');
            // $table->primary(['id_plan_purchased', 'company_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_purchased');
    }
}
