<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan', function (Blueprint $table) {
            $table->integer('id_plan', true);
            $table->integer('number_token')->nullable();
            $table->double('price', 12, 2)->nullable();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->dateTime('date_created')->nullable()->useCurrent();
            $table->dateTime('date_publish')->nullable();
            $table->dateTime('date_expired')->nullable();
            $table->integer('buy_limit')->nullable();
            $table->integer('deleted')->nullable()->default(0);
            $table->dateTime('date_deleted')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan');
    }
}
