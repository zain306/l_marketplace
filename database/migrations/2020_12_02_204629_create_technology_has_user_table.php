<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechnologyHasUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technology_has_user', function (Blueprint $table) {
            $table->integer('technology_id')->index('fk_technology_has_user_technology1_idx');
            $table->integer('user_id')->index('fk_technology_has_user_user1_idx');
            $table->integer('technology_user_link_type_id')->index('fk_technology_has_user_technology_user_link_type1_idx');
            $table->primary(['technology_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technology_has_user');
    }
}
