<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechnologyHasWorkingExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technology_has_working_experience', function (Blueprint $table) {
            $table->integer('technology_id_technology')->index('fk_technology_has_working_experience_technology1_idx');
            $table->integer('working_experience_id_working_experience')->index('fk_technology_has_working_experience_working_experience1_idx');
            // $table->primary(['technology_id_technology', 'working_experience_id_working_experience'],'technology_id_technology');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technology_has_working_experience');
    }
}
