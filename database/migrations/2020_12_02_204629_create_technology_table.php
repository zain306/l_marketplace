<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechnologyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technology', function (Blueprint $table) {
            $table->integer('id_technology', true);
            $table->string('name_technology')->nullable();
            $table->integer('deleted')->nullable()->default(0);
            $table->dateTime('date_deleted')->nullable();
            $table->dateTime('date_create')->nullable()->useCurrent();
            $table->dateTime('date_update')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technology');
    }
}
