<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('token', function (Blueprint $table) {
            $table->integer('id_token', true);
            $table->string('token')->nullable();
            $table->integer('deleted')->nullable();
            $table->dateTime('date_deleted')->nullable();
            $table->dateTime('date_create')->nullable();
            $table->integer('user_id')->index('fk_token_supplier_resource1_idx');
            $table->integer('is_mobile')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('token');
    }
}
