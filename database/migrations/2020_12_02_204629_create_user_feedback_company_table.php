<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserFeedbackCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_feedback_company', function (Blueprint $table) {
            $table->integer('user_id')->index('fk_user_has_company_user1_idx');
            $table->integer('company_id')->index('fk_user_has_company_company1_idx');
            $table->integer('feedback')->nullable();
            $table->primary(['user_id', 'company_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_feedback_company');
    }
}
