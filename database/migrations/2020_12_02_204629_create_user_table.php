<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('deleted')->nullable();
            $table->dateTime('date_deleted')->nullable();
            $table->dateTime('date_create')->nullable();
            $table->dateTime('date_update')->nullable();
            $table->integer('user_type_id')->index('fk_user_user_type1_idx');
            $table->integer('company_id')->index('fk_user_company1_idx');
            $table->integer('city_live')->index('fk_user_city1_idx');
            $table->integer('is_available')->nullable();
            $table->dateTime('available from')->nullable();
            $table->dateTime('available_to')->nullable();
            $table->float('day_rate', 10, 0)->nullable();
            $table->float('day_cost', 10, 0)->nullable();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
