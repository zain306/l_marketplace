<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTypeHasPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_type_has_plan', function (Blueprint $table) {
            $table->integer('user_type_id')->index('fk_user_type_has_plan_user_type1_idx');
            $table->integer('plan_id')->index('fk_user_type_has_plan_plan1_idx');
            $table->primary(['user_type_id', 'plan_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_type_has_plan');
    }
}
