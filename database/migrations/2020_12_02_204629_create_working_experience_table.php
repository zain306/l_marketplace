<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkingExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working_experience', function (Blueprint $table) {
            $table->integer('id_working_experience', true);
            $table->integer('deleted')->nullable();
            $table->dateTime('date_deleted')->nullable();
            $table->dateTime('date_create')->nullable();
            $table->integer('user_id')->index('fk_working_experience_supplier_resource1_idx');
            $table->dateTime('start_date');
            $table->dateTime('end_date')->nullable();
            $table->string('title', 100);
            $table->string('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('working_experience');
    }
}
