<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsToClientNeed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_need', function (Blueprint $table) {
            $table->integer('is_active')->default('0')->change();
            $table->integer('deleted')->default('0')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_need', function (Blueprint $table) {
            $table->integer('is_active')->change();
            $table->integer('deleted')->change();
        });
    }
}
