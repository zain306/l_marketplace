<?php

use App\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cityArray = array('Rome', 'Milan', 'Naples', 'Turin', 'Palermo', 'Genoa', 'Bologna', 'Florence', 'Bari', 'Catania', 'Cagliari', 'Venice', 'Verona', 'Messina', 'Padova', 'Trieste', 'Brescia', 'Taranto', 'Parma', 'Prato', 'Modena', 'Reggio di Calabria', 'Reggio Emilia', 'Perugia', 'Livorno', 'Ravenna', 'Foggia', 'Rimini', 'Salerno', 'Ferrara', 'Latina', 'Monza', 'Giugliano in Campania', 'Pescara', 'Bergamo', 'Siracusa');
        for ($i = 0; $i < sizeof($cityArray); $i++) {
            City::create([
                'id_city' => $i + 1,
                'city_name' => $cityArray[$i],
                'deleted' => NULL,
                'date_deleted' => NULL
            ]);
        }
    }
}
