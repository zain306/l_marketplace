<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CreateAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = Hash::make("admin@123");
        $insertUser = User::create([
            'name' => "admin",
            'firstname' => "admin",
            'lastname' => "",
            'email' => "admin@admin.com",
            'password' => $password,
        ]);
        $insertUser->userRole()->insert([
            'user_id' => $insertUser->id,
            'role_id' => "1"
        ]);
    }
}
