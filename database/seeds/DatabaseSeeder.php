<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CitySeeder::class,
            JobSectorSeeder::class,
            PlanSeeder::class,
            RoleSeeder::class,
            TechnologySeeder::class,
            UserTypeSeeder::class,
            CreateAdminSeeder::class,
        ]);
    }
}
