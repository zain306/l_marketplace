<?php

use App\JobSector;
use Illuminate\Database\Seeder;

class JobSectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobName = array('Banking', 'Insurance', 'Telco',);
        $jobCreatedDates = array('2020-12-01 18:24:53', '2020-12-01 18:24:53', '2020-12-01 18:24:53');
        for ($i = 0; $i < sizeof($jobName); $i++) {
            JobSector::create([
                'id_job_sector' => $i + 1,
                'name_job_sector' => $jobName[$i],
                'deleted' => 0,
                'date_deleted' => NULL,
                'date_create' => $jobCreatedDates[$i],
                'date_update' => NULL
            ]);
        }
    }
}
