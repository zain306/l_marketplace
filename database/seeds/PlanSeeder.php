<?php

use App\Plan;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $planNumber = array(100, 300, 500);
        $planPrice = array(5.00, 10.00, 15.00);
        $planTitle = array('Small', 'Medium', 'Large');
        $planDescription = array('Micro', 'PMI', 'Enterprise');
        $planCreatedDates = array('2020-12-01 17:01:29', '2020-12-01 17:01:29', '2020-12-01 17:01:29');
        for ($i = 0; $i < sizeof($planNumber); $i++) {
            Plan::create([
                'id_plan' => $i + 1,
                'number_token' => $planNumber[$i],
                'price' => $planPrice[$i],
                'title' => $planTitle[$i],
                'description' => $planDescription[$i],
                'date_created' => $planCreatedDates[$i],
                'date_publish' => NULL,
                'date_expired' => NULL,
                'buy_limit' => NULL,
                'deleted' => 0,
                'date_deleted' => NULL
            ]);
        }
    }
}
