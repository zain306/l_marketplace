<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleName = array('Administrator', 'Client', 'Supplier');
        $roleCreatedDates = array('2020-12-01 16:59:10', '2020-12-01 16:59:10', '2020-12-01 16:59:10');
        for ($i = 0; $i < sizeof($roleName); $i++) {
            Role::create([
                'id_role' => $i + 1,
                'name_role' => $roleName[$i],
                'date_create' => $roleCreatedDates[$i],
                'deleted' => 0,
                'date_deleted' => NULL,
                'date_update' => NULL
            ]);
        }
    }
}
