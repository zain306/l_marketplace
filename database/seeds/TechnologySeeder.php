<?php

use App\Technology;
use Illuminate\Database\Seeder;

class TechnologySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $technologyName = array('JAVA', '.NET', 'Javascript', 'Project Management');
        $technologyCreatedDates = array('2020-12-01 16:57:39', '2020-12-01 16:57:39', '2020-12-01 16:57:39', '2020-12-01 16:57:39');
        for ($i = 0; $i < sizeof($technologyName); $i++) {
            Technology::create([
                'id_technology' => $i + 1,
                'name_technology' => $technologyName[$i],
                'deleted' => 0,
                'date_deleted' => NULL,
                'date_create' => $technologyCreatedDates[$i],
                'date_update' => NULL
            ]);
        }
    }
}
