<?php

use App\UserType;
use Illuminate\Database\Seeder;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userTypeName = array('Company Administrator', 'Employee', 'Contact point');
        $userTypeCreatedDates = array('2020-12-01 19:14:26', '2020-12-01 19:14:26', '2020-12-01 19:14:26');
        for ($i = 0; $i < sizeof($userTypeName); $i++) {
            UserType::create([
                'id_user_type' => $i + 1,
                'description' => $userTypeName[$i],
                'deleted' => 0,
                'date_deleted' => NULL,
                'date_create' => $userTypeCreatedDates[$i],
                'date_update' => NULL
            ]);
        }
    }
}
