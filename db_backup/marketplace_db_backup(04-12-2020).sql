-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 04, 2020 at 09:12 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketplace_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id_city` int(11) NOT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id_city`, `city_name`, `deleted`, `date_deleted`) VALUES
(1, 'Rome', NULL, NULL),
(2, 'Milan', NULL, NULL),
(3, 'Naples', NULL, NULL),
(4, 'Turin', NULL, NULL),
(5, 'Palermo', NULL, NULL),
(6, 'Genoa', NULL, NULL),
(7, 'Bologna', NULL, NULL),
(8, 'Florence', NULL, NULL),
(9, 'Bari', NULL, NULL),
(10, 'Catania', NULL, NULL),
(11, 'Cagliari', NULL, NULL),
(12, 'Venice', NULL, NULL),
(13, 'Verona', NULL, NULL),
(14, 'Messina', NULL, NULL),
(15, 'Padova', NULL, NULL),
(16, 'Trieste', NULL, NULL),
(17, 'Brescia', NULL, NULL),
(18, 'Taranto', NULL, NULL),
(19, 'Parma', NULL, NULL),
(20, 'Prato', NULL, NULL),
(21, 'Modena', NULL, NULL),
(22, 'Reggio di Calabria', NULL, NULL),
(23, 'Reggio Emilia', NULL, NULL),
(24, 'Perugia', NULL, NULL),
(25, 'Livorno', NULL, NULL),
(26, 'Ravenna', NULL, NULL),
(27, 'Foggia', NULL, NULL),
(28, 'Rimini', NULL, NULL),
(29, 'Salerno', NULL, NULL),
(30, 'Ferrara', NULL, NULL),
(31, 'Latina', NULL, NULL),
(32, 'Monza', NULL, NULL),
(33, 'Giugliano in Campania', NULL, NULL),
(34, 'Pescara', NULL, NULL),
(35, 'Bergamo', NULL, NULL),
(36, 'Siracusa', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city_available_employee`
--

CREATE TABLE `city_available_employee` (
  `city_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client_has_employee_promoted`
--

CREATE TABLE `client_has_employee_promoted` (
  `need_resource_id` int(11) NOT NULL,
  `user_id_employee` int(11) NOT NULL COMMENT 'id dell''utente di tipo employee promosso',
  `user_author_promotion_id` int(11) NOT NULL COMMENT 'id dell''utente che promuove l''employee',
  `date_promotion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client_need`
--

CREATE TABLE `client_need` (
  `id_client_need` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `job_sector_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `remote_work` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `date_delete` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client_need_has_city`
--

CREATE TABLE `client_need_has_city` (
  `client_need_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client_need_has_technology`
--

CREATE TABLE `client_need_has_technology` (
  `client_need_id` int(11) NOT NULL,
  `technology_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id_company` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `vat_number` varchar(30) DEFAULT NULL,
  `date_of_fondation` datetime DEFAULT NULL,
  `number_of_employees` int(11) DEFAULT NULL,
  `number_of_external_resources` int(11) DEFAULT NULL,
  `address_main_office` varchar(255) DEFAULT NULL,
  `path_registration_document` varchar(255) DEFAULT NULL,
  `id_city_head_quarter` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `deleted` int(11) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company_has_city`
--

CREATE TABLE `company_has_city` (
  `company_id` int(10) UNSIGNED ZEROFILL NOT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_sector`
--

CREATE TABLE `job_sector` (
  `id_job_sector` int(11) NOT NULL,
  `name_job_sector` varchar(255) DEFAULT NULL,
  `deleted` int(1) DEFAULT 0,
  `date_deleted` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT current_timestamp(),
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_sector`
--

INSERT INTO `job_sector` (`id_job_sector`, `name_job_sector`, `deleted`, `date_deleted`, `date_create`, `date_update`) VALUES
(1, 'Banking', 0, NULL, '2020-12-01 18:24:53', NULL),
(2, 'Insurance', 0, NULL, '2020-12-01 18:24:53', NULL),
(3, 'Telco', 0, NULL, '2020-12-01 18:24:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_12_02_204629_create_city_table', 0),
(2, '2020_12_02_204629_create_city_available_employee_table', 0),
(3, '2020_12_02_204629_create_client_has_employee_promoted_table', 0),
(4, '2020_12_02_204629_create_client_need_table', 0),
(5, '2020_12_02_204629_create_client_need_has_city_table', 0),
(6, '2020_12_02_204629_create_client_need_has_technology_table', 0),
(7, '2020_12_02_204629_create_company_table', 0),
(8, '2020_12_02_204629_create_company_has_city_table', 0),
(9, '2020_12_02_204629_create_job_sector_table', 0),
(10, '2020_12_02_204629_create_need_resource_table', 0),
(11, '2020_12_02_204629_create_need_resource_has_technology_table', 0),
(12, '2020_12_02_204629_create_plan_table', 0),
(13, '2020_12_02_204629_create_plan_purchased_table', 0),
(14, '2020_12_02_204629_create_role_table', 0),
(15, '2020_12_02_204629_create_role_has_user_table', 0),
(16, '2020_12_02_204629_create_technology_table', 0),
(17, '2020_12_02_204629_create_technology_has_user_table', 0),
(18, '2020_12_02_204629_create_technology_has_working_experience_table', 0),
(19, '2020_12_02_204629_create_technology_user_link_type_table', 0),
(20, '2020_12_02_204629_create_token_table', 0),
(21, '2020_12_02_204629_create_user_table', 0),
(22, '2020_12_02_204629_create_user_feedback_company_table', 0),
(23, '2020_12_02_204629_create_user_type_table', 0),
(24, '2020_12_02_204629_create_user_type_has_plan_table', 0),
(25, '2020_12_02_204629_create_working_experience_table', 0),
(27, '2014_10_12_100000_create_password_resets_table', 1),
(28, '2019_08_19_000000_create_failed_jobs_table', 1),
(29, '2014_10_12_000000_create_users_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `need_resource`
--

CREATE TABLE `need_resource` (
  `id_need_resource` int(11) NOT NULL,
  `year_expirence` int(11) DEFAULT NULL,
  `client_need_id` int(11) NOT NULL,
  `past_expirence` int(11) DEFAULT NULL,
  `seniority` int(11) DEFAULT NULL,
  `day_rate_max` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `need_resource_has_technology`
--

CREATE TABLE `need_resource_has_technology` (
  `need_resource_id_need_resource` int(11) NOT NULL,
  `technology_id_technology` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE `plan` (
  `id_plan` int(11) NOT NULL,
  `number_token` int(11) DEFAULT NULL,
  `price` double(12,2) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT current_timestamp(),
  `date_publish` datetime DEFAULT NULL,
  `date_expired` datetime DEFAULT NULL,
  `buy_limit` int(11) DEFAULT NULL,
  `deleted` int(1) DEFAULT 0,
  `date_deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`id_plan`, `number_token`, `price`, `title`, `description`, `date_created`, `date_publish`, `date_expired`, `buy_limit`, `deleted`, `date_deleted`) VALUES
(1, 100, 5.00, 'Small', 'Micro ', '2020-12-01 17:01:29', NULL, NULL, NULL, 0, NULL),
(2, 300, 10.00, 'Medium', 'PMI', '2020-12-01 17:01:29', NULL, NULL, NULL, 0, NULL),
(3, 500, 15.00, 'Large', 'Enterprise', '2020-12-01 17:01:29', NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plan_purchased`
--

CREATE TABLE `plan_purchased` (
  `id_plan_purchased` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `date_purchase` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `name_role` varchar(255) DEFAULT NULL,
  `date_create` datetime DEFAULT current_timestamp(),
  `deleted` int(1) DEFAULT 0,
  `date_deleted` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `name_role`, `date_create`, `deleted`, `date_deleted`, `date_update`) VALUES
(1, 'Administrator', '2020-12-01 16:59:10', 0, NULL, NULL),
(2, 'Client', '2020-12-01 16:59:10', 0, NULL, NULL),
(3, 'Supplier', '2020-12-01 16:59:10', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_has_user`
--

CREATE TABLE `role_has_user` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `technology`
--

CREATE TABLE `technology` (
  `id_technology` int(11) NOT NULL,
  `name_tecnology` varchar(255) DEFAULT NULL,
  `deleted` int(1) DEFAULT 0,
  `date_deleted` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT current_timestamp(),
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `technology`
--

INSERT INTO `technology` (`id_technology`, `name_tecnology`, `deleted`, `date_deleted`, `date_create`, `date_update`) VALUES
(1, 'JAVA ', 0, NULL, '2020-12-01 16:57:39', NULL),
(2, '.NET', 0, NULL, '2020-12-01 16:57:39', NULL),
(3, 'Javascript', 0, NULL, '2020-12-01 16:57:39', NULL),
(4, 'Project Management', 0, NULL, '2020-12-01 16:57:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `technology_has_user`
--

CREATE TABLE `technology_has_user` (
  `technology_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `technology_user_link_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `technology_has_working_experience`
--

CREATE TABLE `technology_has_working_experience` (
  `technology_id_technology` int(11) NOT NULL,
  `working_experience_id_working_experience` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `technology_user_link_type`
--

CREATE TABLE `technology_user_link_type` (
  `id_technology_user_link_type` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id_token` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `is_mobile` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` int(11) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `user_type_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `city_live` int(11) DEFAULT NULL,
  `is_available` int(11) DEFAULT NULL,
  `available from` datetime DEFAULT NULL,
  `available_to` datetime DEFAULT NULL,
  `day_rate` double DEFAULT NULL,
  `day_cost` double DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `firstname`, `lastname`, `email`, `email_verified_at`, `password`, `deleted`, `date_deleted`, `date_create`, `date_update`, `user_type_id`, `company_id`, `city_live`, `is_available`, `available from`, `available_to`, `day_rate`, `day_cost`, `remember_token`) VALUES
(4, 'zain', NULL, NULL, 'zain55337@gmail.com', NULL, '$2y$10$n/ze20iM4RJ3xEdgoHpwPuIRVFmDYIHlIGiydBfyQHYZyMbyCVzrS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_feedback_company`
--

CREATE TABLE `user_feedback_company` (
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `feedback` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `id_user_type` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` int(1) DEFAULT 0,
  `date_deleted` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT current_timestamp(),
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id_user_type`, `description`, `deleted`, `date_deleted`, `date_create`, `date_update`) VALUES
(1, 'Company Administrator', 0, NULL, '2020-12-01 19:14:26', NULL),
(2, 'Employee', 0, NULL, '2020-12-01 19:14:26', NULL),
(3, 'Contact point', 0, NULL, '2020-12-01 19:14:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_type_has_plan`
--

CREATE TABLE `user_type_has_plan` (
  `user_type_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `working_experience`
--

CREATE TABLE `working_experience` (
  `id_working_experience` int(11) NOT NULL,
  `deleted` int(11) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id_city`);

--
-- Indexes for table `city_available_employee`
--
ALTER TABLE `city_available_employee`
  ADD PRIMARY KEY (`city_id`,`user_id`),
  ADD KEY `fk_city_has_user_user1_idx` (`user_id`),
  ADD KEY `fk_city_has_user_city1_idx` (`city_id`);

--
-- Indexes for table `client_has_employee_promoted`
--
ALTER TABLE `client_has_employee_promoted`
  ADD PRIMARY KEY (`need_resource_id`,`user_id_employee`),
  ADD KEY `fk_client_need_has_user_user1_idx` (`user_id_employee`),
  ADD KEY `fk_client_has_employee_promoted_user1_idx` (`user_author_promotion_id`);

--
-- Indexes for table `client_need`
--
ALTER TABLE `client_need`
  ADD PRIMARY KEY (`id_client_need`),
  ADD KEY `fk_client_need_job_sector1_idx` (`job_sector_id`),
  ADD KEY `fk_client_need_user1_idx` (`user_id`);

--
-- Indexes for table `client_need_has_city`
--
ALTER TABLE `client_need_has_city`
  ADD PRIMARY KEY (`client_need_id`,`city_id`),
  ADD KEY `fk_client_need_has_city_city1_idx` (`city_id`),
  ADD KEY `fk_client_need_has_city_client_need1_idx` (`client_need_id`);

--
-- Indexes for table `client_need_has_technology`
--
ALTER TABLE `client_need_has_technology`
  ADD PRIMARY KEY (`client_need_id`,`technology_id`),
  ADD KEY `fk_client_need_has_technology_technology1_idx` (`technology_id`),
  ADD KEY `fk_client_need_has_technology_client_need1_idx` (`client_need_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id_company`),
  ADD KEY `fk_company_city1_idx` (`id_city_head_quarter`);

--
-- Indexes for table `company_has_city`
--
ALTER TABLE `company_has_city`
  ADD PRIMARY KEY (`company_id`,`city_id`),
  ADD KEY `fk_company_has_city_city1_idx` (`city_id`),
  ADD KEY `fk_company_has_city_company_idx` (`company_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_sector`
--
ALTER TABLE `job_sector`
  ADD PRIMARY KEY (`id_job_sector`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `need_resource`
--
ALTER TABLE `need_resource`
  ADD PRIMARY KEY (`id_need_resource`),
  ADD KEY `fk_need_resource_client_need1_idx` (`client_need_id`);

--
-- Indexes for table `need_resource_has_technology`
--
ALTER TABLE `need_resource_has_technology`
  ADD PRIMARY KEY (`need_resource_id_need_resource`,`technology_id_technology`),
  ADD KEY `fk_need_resource_has_technology_technology1_idx` (`technology_id_technology`),
  ADD KEY `fk_need_resource_has_technology_need_resource1_idx` (`need_resource_id_need_resource`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id_plan`);

--
-- Indexes for table `plan_purchased`
--
ALTER TABLE `plan_purchased`
  ADD PRIMARY KEY (`id_plan_purchased`,`company_id`),
  ADD KEY `fk_plan_purchased_plan1_idx` (`plan_id`),
  ADD KEY `fk_plan_purchased_company1_idx` (`company_id`),
  ADD KEY `fk_plan_purchased_user1_idx` (`user_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `role_has_user`
--
ALTER TABLE `role_has_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `fk_role_has_user_user1_idx` (`user_id`),
  ADD KEY `fk_role_has_user_role1_idx` (`role_id`);

--
-- Indexes for table `technology`
--
ALTER TABLE `technology`
  ADD PRIMARY KEY (`id_technology`);

--
-- Indexes for table `technology_has_user`
--
ALTER TABLE `technology_has_user`
  ADD PRIMARY KEY (`technology_id`,`user_id`),
  ADD KEY `fk_technology_has_user_user1_idx` (`user_id`),
  ADD KEY `fk_technology_has_user_technology1_idx` (`technology_id`),
  ADD KEY `fk_technology_has_user_technology_user_link_type1_idx` (`technology_user_link_type_id`);

--
-- Indexes for table `technology_has_working_experience`
--
ALTER TABLE `technology_has_working_experience`
  ADD PRIMARY KEY (`technology_id_technology`,`working_experience_id_working_experience`),
  ADD KEY `fk_technology_has_working_experience_working_experience1_idx` (`working_experience_id_working_experience`),
  ADD KEY `fk_technology_has_working_experience_technology1_idx` (`technology_id_technology`);

--
-- Indexes for table `technology_user_link_type`
--
ALTER TABLE `technology_user_link_type`
  ADD PRIMARY KEY (`id_technology_user_link_type`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id_token`),
  ADD KEY `fk_token_supplier_resource1_idx` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `fk_user_user_type1_idx` (`user_type_id`),
  ADD KEY `fk_user_company1_idx` (`company_id`),
  ADD KEY `fk_user_city1_idx` (`city_live`);

--
-- Indexes for table `user_feedback_company`
--
ALTER TABLE `user_feedback_company`
  ADD PRIMARY KEY (`user_id`,`company_id`),
  ADD KEY `fk_user_has_company_company1_idx` (`company_id`),
  ADD KEY `fk_user_has_company_user1_idx` (`user_id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id_user_type`);

--
-- Indexes for table `user_type_has_plan`
--
ALTER TABLE `user_type_has_plan`
  ADD PRIMARY KEY (`user_type_id`,`plan_id`),
  ADD KEY `fk_user_type_has_plan_plan1_idx` (`plan_id`),
  ADD KEY `fk_user_type_has_plan_user_type1_idx` (`user_type_id`);

--
-- Indexes for table `working_experience`
--
ALTER TABLE `working_experience`
  ADD PRIMARY KEY (`id_working_experience`),
  ADD KEY `fk_working_experience_supplier_resource1_idx` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id_city` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `client_need`
--
ALTER TABLE `client_need`
  MODIFY `id_client_need` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id_company` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_sector`
--
ALTER TABLE `job_sector`
  MODIFY `id_job_sector` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `need_resource`
--
ALTER TABLE `need_resource`
  MODIFY `id_need_resource` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plan`
--
ALTER TABLE `plan`
  MODIFY `id_plan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `plan_purchased`
--
ALTER TABLE `plan_purchased`
  MODIFY `id_plan_purchased` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `technology`
--
ALTER TABLE `technology`
  MODIFY `id_technology` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `technology_user_link_type`
--
ALTER TABLE `technology_user_link_type`
  MODIFY `id_technology_user_link_type` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id_token` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id_user_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `working_experience`
--
ALTER TABLE `working_experience`
  MODIFY `id_working_experience` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
