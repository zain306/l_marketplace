<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body style="font-family: Arial; font-size: 12px;">
    <div id="app">
        <main class="py-4">
            <div class="container">

                <div class="row justify-content-center">
                    <div style="">
                        <div class="card" style="">
                            <p>
                            {{__('responses.forgot_password_email_statement_1')}}<br>
                            {{__('responses.forgot_password_email_statement_2')}}
                            </p>

                            <p style="font-weight:bold;font-size:16px">
                                <a href="{{ $link }}">
                                   {{__('responses.reset_password')}}
                                </a>
                            </p>

                            <p><span style="color:red;font-weight:bold;">{{__('responses.note')}}:</span> {{__('responses.forgot_password_email_statement_3')}}</p>

                            <p>
                            {{__('responses.forgot_password_email_statement_4')}}
                            </p>

                            <p>
                            {{__('responses.regards')}},<br>
                                {{config()->get("constants.EMAIL_REGARDS_NAME")}}
                            </p>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>

</html>