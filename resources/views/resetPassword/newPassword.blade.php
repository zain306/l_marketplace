<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{__('responses.reset_password')}}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .displayNone {
            display: none;
        }

        .displayBlock {
            display: content;
        }
    </style>
</head>

<body>
    <div id="app">
        <main class="py-4">
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <p style="text-align: center;font-size: 2.5em;font-weight: bold;"> {{config()->get("constants.PROJECT_NAME")}}</p>
                        <div class="card">
                            <div class="card-header">{{__('responses.reset_password')}}</div>

                            <div class="card-body">
                                <div class="errorMsg displayNone">
                                    <div class="alert alert-danger alert-dismissable">
                                        <strong>{{__('responses.error')}}: </strong><span id="eMsg"></span>
                                    </div>
                                </div>
                                <div class="successMsg displayNone">
                                    <div class="alert alert-success alert-dismissable">
                                        <strong>{{__('responses.success')}}! </strong><span id="sMsg"></span>
                                    </div>
                                </div>
                                <div class="resetForm">
                                    <form method="POST" action="javascript:;" id="reset_password">
                                        @csrf

                                        <input type="hidden" id="token" name="token" value="{{ $token }}">
                                        <div class="form-group row">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">{{__('responses.new_password')}}</label>
                                            <div class="col-md-6">
                                                <input type="password" name="password" class="form-control" id="password" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{__('responses.confirm_new_password')}}</label>
                                            <div class="col-md-6">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="resetP btn btn-primary">
                                                {{__('responses.reset_password')}}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script src="{{ url('js/jquery.js')}}"></script>
    <script>
        $(document).ready(function() {

            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });
            $(document).on('submit', "#reset_password", function() {
                $.ajax({
                    type: "POST",
                    url: "{{url('updatePassword')}}",
                    data: {
                        token: $("#token").val(),
                        password: $("#password").val(),
                        confirmPassword: $("#password-confirm").val()
                    },
                    success: function(response) {
                        if (response.success == false) {
                            console.log(response.message);
                            $('.errorMsg').removeClass("displayNone");
                            $('.errorMsg').addClass("displayBlock");
                            $('.successMsg').removeClass("displayBlock");
                            $('.successMsg').addClass("displayNone");
                            $('.resetForm').removeClass("displayNone");
                            $('.resetForm').addClass("displayBlock");
                            $('#eMsg').html(response.message);
                        } else {
                            $('.errorMsg').removeClass("displayBlock");
                            $('.errorMsg').addClass("displayNone");
                            $('.successMsg').removeClass("displayNone");
                            $('.successMsg').addClass("displayBlock");
                            $('.resetForm').removeClass("displayBlock");
                            $('.resetForm').addClass("displayNone");
                            $('#sMsg').html(response.message);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert("error");
                    }
                });
            });
        });
    </script>
</body>


</html>