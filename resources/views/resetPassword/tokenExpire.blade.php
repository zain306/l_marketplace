<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{__('responses.token_expired')}}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .displayNone {
            display: none;
        }

        .displayBlock {
            display: content;
        }
    </style>
</head>

<body>
    <div id="app">
        <main class="py-4">
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <p style="text-align: center;font-size: 2.5em;font-weight: bold;"> {{config()->get("constants.PROJECT_NAME")}}</p>
                        <div class="card">
                            <div class="card-header">{{__("responses.token_expired")}}</div>

                            <div class="card-body">
                                <div class="errorMsg">
                                    <div class="alert alert-danger alert-dismissable">
                                        <strong>{{__("responses.error")}}: </strong><span id="eMsg"> {{__("responses.token_has_expired")}}.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>


</html>