<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/********************************************** Auth Prefix Route ********************************************************/
Route::prefix('auth')->group(base_path('routes/auth.php'));

/********************************************** Plan Prefix Route ********************************************************/
Route::prefix('plan')->group(base_path('routes/plan.php'));

/********************************************** Technology Prefix Route ********************************************************/
Route::prefix('technology')->group(base_path('routes/technology.php'));

/********************************************** Job Sector Prefix Route ********************************************************/
Route::prefix('jobSector')->group(base_path('routes/jobSector.php'));

/********************************************** Plan Prefix Route ********************************************************/
Route::prefix('city')->group(base_path('routes/city.php'));

/********************************************** Employee Prefix Route ********************************************************/
Route::prefix('employee')->group(base_path('routes/employee.php'));

/********************************************** Working Experience Prefix Route ********************************************************/
Route::prefix('workingExperience')->group(base_path('routes/workingExperience.php'));

/********************************************** Company Prefix Route ********************************************************/
Route::prefix('company')->group(base_path('routes/company.php'));

/********************************************** Client Prefix Route ********************************************************/
Route::prefix('client')->group(base_path('routes/client.php'));
