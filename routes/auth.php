<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/********************************************** Login Route ********************************************************/
Route::post('login', 'Api\AuthController@login');

/********************************************** Logout Route ********************************************************/
Route::post('logout', 'Api\AuthController@logout');

/********************************************** Refresh Token Route ********************************************************/
Route::post('refresh', 'Api\AuthController@refresh');

/********************************************** User Profile Route ********************************************************/
Route::get('userProfile', 'Api\AuthController@userProfile');

/********************************************** Forgot Password Route ********************************************************/
Route::post('forgotPassword', 'Api\AuthController@forgotPassword');

/********************************************** Reset Password Route ********************************************************/
Route::post('resetPassword', 'Api\AuthController@resetPassword');

/********************************************** Forgot Username Route ********************************************************/
Route::post('forgotUsername', 'Api\AuthController@forgotUsername');

/********************************************** Register Route ********************************************************/
Route::post('register', 'Api\AuthController@register');

/********************************************** forgotPassword Route ********************************************************/
Route::post('forgotPassword', 'Api\AuthController@forgotPassword');

/********************************************** Company Detail Route ********************************************************/
Route::get('companyDetail', 'Api\AuthController@companyDetail');

/********************************************** Admin Create Route Route ********************************************************/
// Route::post('admin', 'Api\AuthController@createAdmin');