<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/********************************************** List Route ********************************************************/
Route::get('list', 'Api\CityController@list');