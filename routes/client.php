<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/********************************************** Need Prefix Route ********************************************************/
Route::prefix('need')->group(base_path('routes/need.php'));