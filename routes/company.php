<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/********************************************** List Route ********************************************************/
Route::get('list', 'Api\CompanyController@list');

/********************************************** Add Route ********************************************************/
Route::get('detail', 'Api\CompanyController@detail');

/********************************************** Update Route ********************************************************/
Route::post('update', 'Api\CompanyController@update');

/********************************************** Users Route ********************************************************/
Route::get('users', 'Api\CompanyController@users');

/********************************************** Subscription List Route ********************************************************/
Route::get('subscription/list', 'Api\CompanyController@subscriptionList');

/********************************************** Update Route ********************************************************/
Route::get('subscription/detail', 'Api\CompanyController@subscriptionDetail');