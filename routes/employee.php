<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/********************************************** Add Route ********************************************************/
Route::post('add', 'Api\EmployeeController@add');

/********************************************** Updated Route ********************************************************/
Route::post('update', 'Api\EmployeeController@update');

/********************************************** List Route ********************************************************/
Route::get('list', 'Api\EmployeeController@list');

/********************************************** List Route ********************************************************/
Route::get('detail', 'Api\EmployeeController@detail');

/********************************************** Delete Route ********************************************************/
Route::post('delete', 'Api\EmployeeController@delete');