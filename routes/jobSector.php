<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/********************************************** List Route ********************************************************/
Route::get('list', 'Api\JobSectorController@list');

/********************************************** Add Route ********************************************************/
Route::post('add', 'Api\JobSectorController@add');

/********************************************** Update Route ********************************************************/
Route::post('update', 'Api\JobSectorController@update');

/********************************************** Delete Route ********************************************************/
Route::post('delete', 'Api\JobSectorController@delete');