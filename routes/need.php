<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/********************************************** Create Route ********************************************************/
Route::post('create', 'Api\ClientController@createNeed');

/********************************************** List Route ********************************************************/
Route::get('list', 'Api\ClientController@needList');

/********************************************** Detail Route ********************************************************/
Route::get('detail', 'Api\ClientController@needDetail');

/********************************************** Update Route ********************************************************/
Route::post('update', 'Api\ClientController@updateNeed');

/********************************************** Update Status Route ********************************************************/
Route::post('updateStatus', 'Api\ClientController@updateNeedStatus');

/********************************************** Create Resource Route ********************************************************/
Route::post('createResource', 'Api\ClientController@createNeedResource');

/********************************************** Update Resource Route ********************************************************/
Route::post('updateResource', 'Api\ClientController@updateNeedResource');