<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/********************************************** List Route ********************************************************/
Route::get('list', 'Api\PlanController@list');

/********************************************** Add Route ********************************************************/
Route::post('add', 'Api\PlanController@add');

/********************************************** Update Route ********************************************************/
Route::post('update', 'Api\PlanController@update');

/********************************************** Deleted Route ********************************************************/
Route::post('delete', 'Api\PlanController@deleted');