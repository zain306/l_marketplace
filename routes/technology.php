<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/********************************************** List Route ********************************************************/
Route::get('list', 'Api\TechnologyController@list');

/********************************************** Add Route ********************************************************/
Route::post('add', 'Api\TechnologyController@add');

/********************************************** Update Route ********************************************************/
Route::post('update', 'Api\TechnologyController@update');

/********************************************** Delete Route ********************************************************/
Route::post('delete', 'Api\TechnologyController@delete');