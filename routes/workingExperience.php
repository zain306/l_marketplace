<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/********************************************** Add Route ********************************************************/
Route::post('add', 'Api\WorkingExperienceController@add');

/********************************************** Updated Route ********************************************************/
Route::post('update', 'Api\WorkingExperienceController@update');

/********************************************** List Route ********************************************************/
Route::get('detail', 'Api\WorkingExperienceController@detail');

/********************************************** Delete Route ********************************************************/
Route::post('delete', 'Api\WorkingExperienceController@delete');